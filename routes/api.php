<?php

use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// User route
Route::get('/users', [UserController::class, 'showAll']);
Route::get('/users/{id}', [UserController::class, 'showById']);
Route::post('/users/new', [UserController::class, 'new']);
Route::delete('/users/{id}/delete', [UserController::class, 'delete']);
Route::post('/users/{is}/edit', [UserController::class, 'update']);