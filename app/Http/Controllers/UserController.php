<?php

namespace App\Http\Controllers;

use App\Models\User;
use Facade\FlareClient\Time\Time;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Date;

class UserController extends Controller
{
    /** 
     * Show all users
     * @return report a array of users
     */
    public function showAll() 
    {
      $report = User::all();
      return $report;
    }
    
    /**
     * Show a user using a id
     * @param id the user id
     * @return user
     */
    public function showById($id) 
    {
      $report = User::findOrFail($id);
      return $report;
    }

    /**
     * Create a user
     * @param request {Request} the request body
     * @return response returned code
     */
    public function new(Request $request) 
    {
      $newUser = new User;

      $newUser->name = $request->name;
      $newUser->password = $request->password;
      $newUser->email = $request->email;
      $newUser->isAdmin = $request->isAdmin;
      if ($request -> type) {
        $newUser->type = $request->type;
      }

      $newUser->save();

      return '200';
    }

    /** 
     * Delete a user
     * @param id the id of the user delete
     * @return status
     */
    public function delete($id)
    {
      $newUser = User::findOrFail($id);
      $newUser->delete();
      return '200';
    }

    /**
     * Modify a user
     * @param id the user id
     * @param request the new user
     */
    public function update(Request $request, $id) 
    {
      $newUser = User::findOrFail($id);

      $newUser->name = $request->name;
      $newUser->password = $request->password;
      $newUser->email = $request->email;
      $newUser->isAdmin = $request->isAdmin;
      if ($request -> type) {
        $newUser->type = $request->type;
      }
      $newUser->save();

      return '200';
    }
}
