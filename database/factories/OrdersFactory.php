<?php

namespace Database\Factories;

use App\Models\Orders;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrdersFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Orders::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'clientId' => rand(0, 9),
            'info' => $this->faker->sentence(),
            'total' => $this->faker->numberBetween(1, 100),
            'prepay' => $this->faker->numberBetween(1, 100),
            'toPay' => $this->faker->numberBetween(1, 100)
        ];
    }
}
