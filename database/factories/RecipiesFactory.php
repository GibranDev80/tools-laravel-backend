<?php

namespace Database\Factories;

use App\Models\Recipies;
use Illuminate\Database\Eloquent\Factories\Factory;

class RecipiesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Recipies::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'userId' => rand(0, 10),
            'tagId' => rand(0, 1),
            'name' => $this->faker->name(),
            'description' => $this->faker->sentence(),
            'ingredients' => $this->faker->sentence(),
            'steps' => '# Hello There'
        ];
    }
}
