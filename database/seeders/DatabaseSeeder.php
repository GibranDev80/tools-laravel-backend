<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory(11)->create();
        \App\Models\Tags::factory(2)->create();
        \App\Models\Recipies::factory(2)->create();
        \App\Models\Clients::factory(10)->create(); 
        \App\Models\Orders::factory(2)->create(); 
    }
}
